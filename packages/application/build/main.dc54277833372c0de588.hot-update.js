exports.id = "main";
exports.modules = {

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "../../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _razzle_components_src_Link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @razzle/components/src/Link */ "../components/src/Link.js");
var _jsxFileName = "/Users/suhas/space/razzle-sample/packages/application/src/App.js";



function App() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 7
    }
  }, "I am the ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_razzle_components_src_Link__WEBPACK_IMPORTED_MODULE_1__["default"], {
    href: "/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 18
    }
  }, "Link"), "!"));
}

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ })

};
//# sourceMappingURL=main.dc54277833372c0de588.hot-update.js.map