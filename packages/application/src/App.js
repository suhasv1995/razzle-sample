import React from 'react';
import Link from '@razzle/components/src/Link';

function App() {
  return (
    <div>
      <h1>
        I am the <Link href="/">Link</Link>!
      </h1>
    </div>
  );
}

export default App;
